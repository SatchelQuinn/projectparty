// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPawnInterface.h"
//#include "EPartyEnums.h"

// Add default functionality here for any IPlayerPawnInterface functions that are not pure virtual.

void IPlayerPawnInterface::InitPawnValues_Implementation(int32 index, int32 Lives, EActionType PrimaryButtonAction, EActionType SecondaryButtonAction)
{
}

void IPlayerPawnInterface::MovePlayerTo_Implementation(FVector Location, FRotator FinalRot)
{
}

void IPlayerPawnInterface::Celebrate_Implementation(bool PlayerLost)
{
}

void IPlayerPawnInterface::RegisterModifier_Implementation(APlayerModifierBase* NewMod)
{
}


void IPlayerPawnInterface::DestroyModifiers_Implementation()
{
}

void IPlayerPawnInterface::VoidOut_Implementation()
{
}

void IPlayerPawnInterface::OnEliminate_Implementation()
{
}


