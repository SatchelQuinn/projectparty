// Fill out your copyright notice in the Description page of Project Settings.


#include "StageManager.h"
#include "Kismet/GameplayStatics.h"
#include "PartyGameState.h"

// Sets default values
AStageManager::AStageManager()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

}

// Called when the game starts or when spawned
void AStageManager::BeginPlay()
{
	Super::BeginPlay();
	APartyGameState* GameStateRef = Cast<APartyGameState>(UGameplayStatics::GetGameState(this));
	if (GameStateRef)
	{
		GameStateRef->StageManager = this;
	}

}

// Called every frame
void AStageManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AStageManager::Agitate()
{
	OnAgitate();
}

void AStageManager::OnAgitate_Implementation()
{
}

