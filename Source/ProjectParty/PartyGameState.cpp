// Fill out your copyright notice in the Description page of Project Settings.


#include "PartyGameState.h"
#include "PartyGameInstance.h"
#include "Kismet/GameplayStatics.h"
#include "UnrealNetwork.h"
#include "TimerManager.h"

void APartyGameState::BeginPlay()
{
	Super::BeginPlay();
	if (HasAuthority())
	{
		UPartyGameInstance* Instance = Cast<UPartyGameInstance>(UGameplayStatics::GetGameInstance(this));
		Rules = Instance->DefaultGameRules[Instance->SelectedGame];
		GameType = Instance->GameType;
		BindConditionDelegates();
	}
	
}

void APartyGameState::BindConditionDelegates()
{
	SomeTimePassedDel;
	SomeTimePassedDel.BindUFunction(this, FName("AgitateStage"));
	TimeHalfUpDel.BindUFunction(this, FName("AgitateStage"));
	TimeAlmostUpDel.BindUFunction(this, FName("AgitateStage"));
}

void APartyGameState::OnPlayerElimated_Implementation(APartyPlayerState* PlayerState)
{
}

void APartyGameState::StartMinigame()
{
	bGameStarted = true;
	GetWorldTimerManager().SetTimer(MinigameTimer, this, &APartyGameState::EndMinigame, Rules.TimerLength, false);
	//Set the condition timers
	GetWorldTimerManager().SetTimer(SomeTimePassedTimer, SomeTimePassedDel, Rules.TimerLength*.25, false);
	GetWorldTimerManager().SetTimer(TimeHalfUpTimer, TimeHalfUpDel, Rules.TimerLength*.5, false);
	GetWorldTimerManager().SetTimer(TimeAlmostUpTimer, TimeAlmostUpDel, Rules.TimerLength*.75, false);
	OnMinigameStarted();
}

void APartyGameState::OnMinigameStarted_Implementation()
{
}

void APartyGameState::EndMinigame()
{
	bGameStarted = false;
	OnMinigameEnded();
}

void APartyGameState::OnMinigameEnded_Implementation()
{
}

FRulesStruct APartyGameState::GetGameRules(AActor* Context)
{
	AGameStateBase* GameState = UGameplayStatics::GetGameState(Context);
	APartyGameState* PartyGameState = Cast<APartyGameState>(GameState);
	if (PartyGameState)
	{
		return PartyGameState->Rules;
	}
	return FRulesStruct();
}

void APartyGameState::AgitateStage()
{
	if (StageManager)
	{
		StageManager->Agitate();
	}
}


void APartyGameState::GetLifetimeReplicatedProps(TArray< FLifetimeProperty >& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(APartyGameState, Rules);
	DOREPLIFETIME(APartyGameState, GameType);
};