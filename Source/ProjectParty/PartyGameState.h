// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"
#include "GameplayStructures.h"
#include "StageManager.h"
#include "PartyGameState.generated.h"

/**
 * 
 */
UCLASS()
class PROJECTPARTY_API APartyGameState : public AGameStateBase
{
	GENERATED_BODY()
public:
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Replicated, Category = "Game Info")
		TEnumAsByte<EGameType> GameType;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Replicated, Category = "Game Info")
		FRulesStruct Rules;

	UPROPERTY(BlueprintReadOnly, Category = "Current Game")
		bool bGameStarted = false;

	UPROPERTY(BlueprintReadOnly, Category = "Current Game")
		FTimerHandle MinigameTimer;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Current Game")
		AStageManager* StageManager;

protected:

	FTimerHandle SomeTimePassedTimer;
	FTimerHandle TimeHalfUpTimer;
	FTimerHandle TimeAlmostUpTimer;

	FTimerDelegate SomeTimePassedDel;
	FTimerDelegate TimeHalfUpDel;
	FTimerDelegate TimeAlmostUpDel;

public:

	virtual void BeginPlay();

	void BindConditionDelegates();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Gameplay")
		void OnPlayerElimated(APartyPlayerState* PlayerState);
	void OnPlayerElimated_Implementation(APartyPlayerState* PlayerState);

	UFUNCTION(BlueprintCallable, Category = "Gameplay")
		void StartMinigame();

	UFUNCTION(BlueprintNativeEvent, Category = "Gameplay")
		void OnMinigameStarted();
	void OnMinigameStarted_Implementation();

	UFUNCTION(BlueprintCallable, Category = "Gameplay")
		void EndMinigame();

	UFUNCTION(BlueprintNativeEvent, Category = "Gameplay")
		void OnMinigameEnded();
	void OnMinigameEnded_Implementation();

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Gameplay")
		static FRulesStruct GetGameRules(AActor* Context);

	//Tell the stage manager to transform the stage
	UFUNCTION(BlueprintCallable, Category = "Gameplay")
		void AgitateStage();

};
