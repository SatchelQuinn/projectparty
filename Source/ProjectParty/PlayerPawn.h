// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "PlayerPawnInterface.h"
#include "EPartyEnums.h"
#include "PlayerPawn.generated.h"

UCLASS()
class PROJECTPARTY_API APlayerPawn : public APawn, public IPlayerPawnInterface
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APlayerPawn();

	//The index of the player. Used for color coding.
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Attributes")
		int32 PlayerIndex;

	//The number of lives the player has. Used for elimination matches
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Replicated, category = "Attributes")
		int32 RemainingLives;

	UPROPERTY(BlueprintReadOnly, category = "Controls")
		TEnumAsByte<EActionType> PrimaryAction = AT_None;

	UPROPERTY(BlueprintReadOnly, category = "Controls")
		TEnumAsByte<EActionType> SecondaryAction = AT_None;

	//What method this character uses for locamotion
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated, category = "Attributes")
		TEnumAsByte<EMovementMethod> MovementMethod;

	//An actor in range to call the interact function
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated, category = "Gameplay")
		AActor* InteractActor;

	//The intract actor we are currently holding. Can only hold one.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated, category = "Gameplay")
		AActor* HeldActor;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	//Called before the pawn is spawned.
	void InitPlayerValues(int32 Index);

	//Interface version of InitPlayerValues function
	virtual void InitPawnValues_Implementation(int32 index, int32 Lives, EActionType PrimaryButtonAction, EActionType SecondaryButtonAction) override;

};
