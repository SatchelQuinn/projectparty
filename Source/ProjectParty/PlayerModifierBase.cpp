// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerModifierBase.h"
#include "UnrealNetwork.h"
#include "Kismet/KismetMathLibrary.h"

// Sets default values
APlayerModifierBase::APlayerModifierBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	bReplicates = true;
}

// Called when the game starts or when spawned
void APlayerModifierBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void APlayerModifierBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void APlayerModifierBase::SetGenericOwner(APawn* IncomingOwner)
{
	GenericOwner = IncomingOwner;
}

void APlayerModifierBase::Equip_Implementation()
{

}

void APlayerModifierBase::GrabOffsetActor(USceneComponent* GrabMesh)
{
	FTransform OffsetTrans = GrabMesh->GetSocketTransform("GrabOffset",RTS_Actor);
	SetActorTransform(UKismetMathLibrary::ComposeTransforms(OffsetTrans, GetActorTransform()));
}

