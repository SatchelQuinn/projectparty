// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PlayerModifierBase.h"
#include "PlayerPawn.h"
#include "PlayerCharacter.h"
#include "PlayerModifier.generated.h"

/**
 * 
 */
UCLASS()
class PROJECTPARTY_API APlayerModifier : public APlayerModifierBase
{
	GENERATED_BODY()
	
public:

	APlayerModifier();

	//which character owns this actor
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated, Meta = (ExposeOnSpawn = true), Category = "Item")
		APlayerCharacter* OwningCharacter;

	//which pawn owns this actor
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated, Meta = (ExposeOnSpawn = true), Category = "Item")
		APlayerPawn* OwningPawn;

	//which pawn is currently picking up this modifier
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated, Category = "Item")
		APawn* PickedUpBy;

	//How far forward we push this actor on spawn 
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Meta = (ExposeOnSpawn = true), Category = "Item")
		FVector ForwardOffset;

protected:

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:

	//returns the correct owning pawn
	UFUNCTION(BlueprintPure, category = "Gameplay")
		APawn* GetOwningPawn();

	//Sets the correct owner varible. Called when spawning
	UFUNCTION(BlueprintCallable)
		void SetOwningPawn();

};
