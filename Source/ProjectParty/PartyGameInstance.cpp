// Fill out your copyright notice in the Description page of Project Settings.


#include "PartyGameInstance.h"
#include "Engine/World.h"

UPartyGameInstance::UPartyGameInstance()
{
	PlayersData.Init(FPlayerData(), 4);
	PlayersData[0].bIsActive = true;
	
}


void UPartyGameInstance::CallServerTravel(FString LevelURL, bool bAbsolute)
{
	GetWorld()->ServerTravel(LevelURL, bAbsolute);
}
