// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerActionInterface.h"

// Add default functionality here for any IPlayerActionInterface functions that are not pure virtual.

bool IPlayerActionInterface::CanInteract_Implementation()
{
	return false;
}

void IPlayerActionInterface::Interact_Implmentation(APawn* Initiator)
{
}

void IPlayerActionInterface::Use_Implmentation(APawn* Initiator, bool bIsForced)
{
}

void IPlayerActionInterface::UseRelease_Implmentation(APawn* Initiator, float HoldTime)
{
}
