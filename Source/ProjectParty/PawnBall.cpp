// Fill out your copyright notice in the Description page of Project Settings.


#include "PawnBall.h"
#include "Components/SphereComponent.h"
#include "Components/InputComponent.h"
#include "Kismet/GameplayStatics.h"

APawnBall::APawnBall()
{
	SphereRadius = 100.f;
	TorqueIntensity = 100000000.f;

	// Create a sphere component.
	SphereCollision = CreateDefaultSubobject<USphereComponent>(TEXT("Collision Sphere"));
	RootComponent = SphereCollision;
	SphereCollision->InitSphereRadius(SphereRadius);
	SphereCollision->SetSimulatePhysics(true);
	SphereCollision->SetCollisionProfileName(TEXT("Pawn"));


}

void APawnBall::CheckFallingStatus()
{
	if (SphereCollision)
	{
		//trace for ground
		FHitResult Hit;
		float RayLength = SphereRadius + 5.f;
		FVector StartLocation = SphereCollision->GetComponentLocation();
		FVector EndLocation = StartLocation + FVector(0.f, 0.f, -RayLength);
		FCollisionQueryParams CollisionParameters;
		CollisionParameters.AddIgnoredActor(this);
		GetWorld()->LineTraceSingleByChannel(Hit, StartLocation, EndLocation, ECollisionChannel::ECC_Pawn, CollisionParameters);
		//DrawDebugLine(GetWorld(), StartLocation, EndLocation, FColor::Green, true, -1, 0, 1.f);
		bIsFalling = !Hit.bBlockingHit;
	}
}

void APawnBall::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	CheckFallingStatus();
}

void APawnBall::BeginPlay()
{
	Super::BeginPlay();
}

void APawnBall::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Set up gameplay key bindings
	check(PlayerInputComponent);
	PlayerInputComponent->BindAxis("MoveForward", this, &APawnBall::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &APawnBall::MoveRight);

}

void APawnBall::MoveForward(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);
		const FRotator CameraRot(0, UGameplayStatics::GetPlayerCameraManager(this, 0)->GetCameraRotation().Yaw, 0);
		FRotator ActorRot(0, GetActorRotation().Yaw, 0);

		// get forward vector
		//const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		FVector Direction = FVector(0.f, 1.f, 0.f);
		switch (MovementMethod)
		{
		case MM_World:
			break;
		case MM_Camera:
			Direction = FRotationMatrix(CameraRot).GetUnitAxis(EAxis::X);
			break;
		case MM_Perspective:
			Direction = FRotationMatrix(ActorRot).GetUnitAxis(EAxis::X);
		case MM_Tank:
			Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		default:
			break;
		}

		AddMovementInput(Direction, Value);
	}
}

void APawnBall::MoveRight(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{

	}
}