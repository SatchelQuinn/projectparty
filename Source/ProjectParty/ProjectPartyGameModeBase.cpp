// Fill out your copyright notice in the Description page of Project Settings.


#include "ProjectPartyGameModeBase.h"
#include "Kismet/GameplayStatics.h"
#include "PlayerCharacter.h"
#include "PlayerPawn.h"
#include "Engine/World.h"
#include "TimerManager.h"
#include "PartyGameInstance.h"


AProjectPartyGameModeBase::AProjectPartyGameModeBase()
{
	//PlayerInitTimerArray.Init(FTimerHandle(), 8);
	//PlayerInitDelegateArray.Init(FTimerDelegate(), 8);
	//ControllerInitArray.Init(nullptr, 8);
}

void AProjectPartyGameModeBase::PostLogin(APlayerController* NewPlayer)
{
	Super::PostLogin(NewPlayer);
	//ReinitializePlayer(NewPlayer);
}

void AProjectPartyGameModeBase::GenericPlayerInitialization(AController* C)
{
	Super::GenericPlayerInitialization(C);
	//int32 initIndex;
	//initIndex = ControllerInitArray.Add(C);
	//Binding the function with specific values
	//PlayerInitDelegateArray[initIndex].BindUFunction(this, FName("ReinitializePlayer"), C);
	//Calling MyUsefulFunction after 5 seconds without looping
	//GetWorld()->GetTimerManager().SetTimer(PlayerInitTimerArray[initIndex], PlayerInitDelegateArray[initIndex], .5f, false);
	//FTimerHandle Han;

	FTimerDelegate Del;
	Del.BindUFunction(this, FName("ReinitializePlayer"), C);
	GetWorldTimerManager().SetTimerForNextTick(Del);
	
	//GetWorld()->GetTimerManager().SetTimer(Han, Del, .5f, false);
	//ReinitializePlayer(C);
}

void AProjectPartyGameModeBase::ReinitializePlayer(AController* C)
{
	APartyController* Controller = Cast<APartyController>(C);
	if (Controller)
	{
		Players.AddUnique(Controller);
		
		FTimerDelegate Del;
		FTimerHandle Han;
		Del.BindUFunction(this, FName("SpawnPlayerModifiers"), SpawnPlayerClass(Controller));
		//GetWorldTimerManager().SetTimer(Han, Del, 5.f, false);
		GetWorldTimerManager().SetTimerForNextTick(Del);
		UE_LOG(LogTemp, Warning, TEXT("Spawning Player Modifiers"));
		UE_LOG(LogTemp, Warning, TEXT("Reinit player %i"), UGameplayStatics::GetPlayerControllerID(Controller));
	}
}

void AProjectPartyGameModeBase::SpawnLocalPlayers(int32 AdditionalPlayers)
{
	for (int i = 0; i < AdditionalPlayers; i++)
	{
		APartyController* NewController = Cast<APartyController>(UGameplayStatics::GetPlayerController(this, i + 1));
		if (!NewController)
		{
			NewController = Cast<APartyController>(UGameplayStatics::CreatePlayer(this, i + 1, true));
		}
		if (NewController)
				Players.AddUnique(NewController);
	}
}

APawn* AProjectPartyGameModeBase::SpawnPlayerClass(APartyController* Controller)
{
	RulesStruct.PlayerPawn;
	APawn* OldPawn = Controller->GetPawn();
	//Spawn new pawn
	FVector Location(0.0f, 0.0f, 0.0f);
	FRotator Rotation(0.0f, 0.0f, 0.0f);
	if (OldPawn)
	{
		Location = OldPawn->GetActorLocation();
		Rotation = OldPawn->GetActorRotation();
	}
	FTransform SpawnTransform(Rotation, Location);
	FActorSpawnParameters SpawnInfo;
	SpawnInfo.bNoFail = true;
	UClass* SpawnPawn = RulesStruct.PlayerPawn;
	APawn* NewPawn = nullptr;
	if (SpawnPawn)
	{
		int32 NewIndex = 0;
		switch(GameType){
		case GT_Local :
			NewIndex = UGameplayStatics::GetPlayerControllerID(Controller);
			break;
		case GT_Online :
			NewIndex = Players.Find(Controller);
			break;
		default:
			NewIndex = Players.Find(Controller);
		}
		
		NewPawn = Cast<APawn>(UGameplayStatics::BeginDeferredActorSpawnFromClass(this, SpawnPawn, SpawnTransform,ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn));
		if (NewPawn)
		{
			IPlayerPawnInterface* IPawn = Cast<IPlayerPawnInterface>(NewPawn);
			if (IPawn)
			{
				IPawn->Execute_InitPawnValues(NewPawn, NewIndex, RulesStruct.Lives, RulesStruct.PrimaryButtonAction, RulesStruct.SecondaryButtonAction);
			}
			/* Old implementation for init varibles
			//We are spawning a character
			APlayerCharacter* NewPlayerCharacter = Cast<APlayerCharacter>(NewPawn);
			if (NewPlayerCharacter)
				NewPlayerCharacter->InitPlayerValues(NewIndex, RulesStruct.Lives);
			//We are spawning a pawn
			else
			{
				APlayerPawn* NewPlayerPawn = Cast<APlayerPawn>(NewPawn);
				if (NewPlayerPawn)
					NewPlayerPawn->InitPlayerValues(NewIndex);
			}
			*/
			UGameplayStatics::FinishSpawningActor(NewPawn, SpawnTransform);
			//possess new pawn and then kill the old
			Controller->Possess(NewPawn);
			if (OldPawn)
			{
				Controller->Possess(NewPawn);
				OldPawn->Destroy();
			}
			/*
			FTimerDelegate Del;
			FTimerHandle Han;
			Del.BindUFunction(this, FName("SpawnPlayerModifiers"),NewPawn);
			//GetWorldTimerManager().SetTimer(Han, Del, 5.f, false);
			GetWorldTimerManager().SetTimerForNextTick(Del);
			UE_LOG(LogTemp, Warning, TEXT("Spawning Player Modifiers"));
			*/
		}
	}
	return NewPawn;
}

void AProjectPartyGameModeBase::BeginPlay()
{
	Super::BeginPlay();
	UPartyGameInstance* Instance = Cast<UPartyGameInstance>(UGameplayStatics::GetGameInstance(this));
	RulesStruct = UnpackRules();
	if (Instance)
	{
		GameType = Instance->GameType;
	}
	SetSplitscreenStatus(this, RulesStruct.bUseSplitscreen);
}

/*
bool AProjectPartyGameModeBase::DespawnPlayer(APartyController* Controller, bool Player1Override)
{

	ACapstoneCharacter* OldPlayer = Cast<ACapstoneCharacter>(Controller->GetPawn());
	if (OldPlayer != nullptr)
	{
		int32 OldIndex;
		OldIndex = OldPlayer->PlayerIndex;
		Players[OldPlayer->PlayerIndex] = nullptr;
		OnPlayerRemoved(OldIndex, OldPlayer->GetActorLocation());
	}
	bool bIsDestroyed;
	APawn* OldPawn = Controller->GetPawn();
	//Spawn new pawn
	FVector Location(0.0f, 0.0f, 0.0f);
	FRotator Rotation(0.0f, 0.0f, 0.0f);
	FActorSpawnParameters SpawnInfo;
	APawn* NewPawn = GetWorld()->SpawnActor<APawn>(APawn::StaticClass(), Location, Rotation, SpawnInfo);
	//possess new pawn and then kill the old
	Controller->Possess(NewPawn);
	bIsDestroyed = OldPawn->Destroy();
	return bIsDestroyed;
}

//Spawn Active Players
	if (Instance)
	{
		int32 i = 0;
		for (FPlayerData pd : Instance->PlayersData)
		{
			APawn* player = Cast<APawn>(UGameplayStatics::GetPlayerPawn(GetWorld(), i));
			APartyController* playerController = Cast<APartyController>(UGameplayStatics::GetPlayerController(GetWorld(), i));
			//If player doesn't exist, spawn one
			if (player == nullptr && playerController && pd.bIsActive)
			{
				APawn* NewPlayer = nullptr;
				SpawnPlayerClass(playerController);
				UE_LOG(LogTemp, Warning, TEXT("Spawned Player"));
			}
			UE_LOG(LogTemp, Warning, TEXT("Spawning active player loop"));
			i++;
		}
	}


*/

void AProjectPartyGameModeBase::SetSplitscreenStatus(AActor* Context, bool Status)
{
	if (Context)
	{
		GetWorld()->GetGameViewport()->SetDisableSplitscreenOverride(!Status);
	}
}

void AProjectPartyGameModeBase::PostServerTravel()
{
	BeginPlay();
}


FRulesStruct AProjectPartyGameModeBase::UnpackRules()
{
	UPartyGameInstance* Instance = Cast<UPartyGameInstance>(UGameplayStatics::GetGameInstance(this));
	return Instance->DefaultGameRules[Instance->SelectedGame];
}

void AProjectPartyGameModeBase::SpawnPlayerModifiers(APawn* PlayerPawn)
{

	if (PlayerPawn)
	{
		FVector Location = PlayerPawn->GetActorLocation()+ FVector(0.f,0.f, 200.f);
		FRotator Rotation = PlayerPawn->GetActorRotation();
		FTransform SpawnTransform(Rotation, Location);
		UClass* Spawn = RulesStruct.PlayerModifier;
		APlayerModifierBase* NewModifier = nullptr;
		if (Spawn)
		{
			NewModifier = Cast<APlayerModifierBase>(UGameplayStatics::BeginDeferredActorSpawnFromClass(this, Spawn, SpawnTransform, ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn));
			if (NewModifier)
			{
				NewModifier->SetGenericOwner(PlayerPawn);
				UGameplayStatics::FinishSpawningActor(NewModifier, SpawnTransform);
				if (NewModifier->bAutoEquip)
					NewModifier->Equip();
				//Send BP Message
				IPlayerPawnInterface* IPawn = Cast<IPlayerPawnInterface>(PlayerPawn);
				if (IPawn)
				{
					IPawn->Execute_RegisterModifier(PlayerPawn, NewModifier);
				}

			}
		}
	}
}