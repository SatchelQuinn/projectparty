// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "PartyPlayerState.generated.h"

/**
 * 
 */
UCLASS()
class PROJECTPARTY_API APartyPlayerState : public APlayerState
{
	GENERATED_BODY()
public:
	APartyPlayerState();
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player")
		bool bIsElimated = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player")
		int32 TeamIndex = 0;

protected:

public:
	virtual void BeginPlay();
	
	UFUNCTION(BlueprintCallable)
		void Eliminate(bool bDisableInput);
};
