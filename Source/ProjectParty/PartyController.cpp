// Fill out your copyright notice in the Description page of Project Settings.


#include "PartyController.h"

void APartyController::Tick(float DeltaTime)
{
	TArray<FKey> Keys;
	KeyTimeMap.GetKeys(Keys);
	for (auto key : Keys)
	{
		if (IsInputKeyDown(key))
		{
			KeyTimeMap.Add(key, GetInputKeyTimeDown(key));
		}
	}
}

void APartyController::RegisterKey(FKey Key)
{
	KeyTimeMap.Add(Key, 0.f);
}

float APartyController::TimeKeyHeldDown(FKey Key)
{
	float* valueptr = KeyTimeMap.Find(Key);
	if (valueptr)
	{
		return *valueptr;
	}
	return 0.0f;
}
