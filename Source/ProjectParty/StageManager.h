// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "StageManager.generated.h"

UCLASS()
class PROJECTPARTY_API AStageManager : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AStageManager();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, category = "Gameplay")
		int32 AgitationLevel;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//Called to distupt the current stage state
	UFUNCTION(BlueprintCallable, category = "Gameplay")
		void Agitate();

	UFUNCTION(BlueprintNativeEvent, category = "Gameplay")
		void OnAgitate();
	void OnAgitate_Implementation();
};
