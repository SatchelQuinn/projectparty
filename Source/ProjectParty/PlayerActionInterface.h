// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "PlayerActionInterface.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UPlayerActionInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class PROJECTPARTY_API IPlayerActionInterface
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Actions")
		bool CanInteract();
	bool CanInteract_Implementation();
	
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Actions")
	void Interact(APawn* Initiator);
	void Interact_Implmentation(APawn* Initiator);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Actions")
		void Use(APawn* Initiator, bool bIsForced);
	void Use_Implmentation(APawn* Initiator, bool bIsForced);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Gameplay")
		void UseRelease(APawn* Initiator, float HoldTime);
	void UseRelease_Implmentation(APawn* Initiator, float HoldTime);
};
