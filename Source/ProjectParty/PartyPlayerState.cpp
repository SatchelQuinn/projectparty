// Fill out your copyright notice in the Description page of Project Settings.


#include "PartyPlayerState.h"
#include "Kismet/GameplayStatics.h"
#include "PlayerPawn.h"
#include "PartyController.h"
#include "PartyGameState.h"

APartyPlayerState::APartyPlayerState()
{

}

void APartyPlayerState::BeginPlay()
{
	Super::BeginPlay();
}

void APartyPlayerState::Eliminate(bool bDisableInput)
{
	//Do nothing if we are already eliminated
	if (bIsElimated)
		return;
	bIsElimated = true;
	if (bDisableInput)
	{
		if (GetPawn())
		{
			AController* Controller = GetPawn()->Controller;
			APlayerController* PController = Cast<APlayerController>(Controller);
			//This pawn is player controlled
			if (PController)
			{
				GetPawn()->DisableInput(PController);
			}
			//TODO Make AI functionality 
			else
			{

			}
			//Send BP Message
			IPlayerPawnInterface* IPawn = Cast<IPlayerPawnInterface>(GetPawn());
			if (IPawn)
			{
				IPawn->Execute_OnEliminate(GetPawn());
			}
		}
	}
	//Tell the game state that we were eliminiated
	APartyGameState* PGameState = Cast<APartyGameState>(UGameplayStatics::GetGameState(this));
	PGameState->OnPlayerElimated(this);
}
