// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "PartyController.generated.h"

/**
 * 
 */
UCLASS()
class PROJECTPARTY_API APartyController : public APlayerController
{
	GENERATED_BODY()

public:

protected:

	TMap<FKey, float> KeyTimeMap;

public:

	virtual void Tick(float DeltaTime) override;
	
	//Register this key with the keytimemap. Key held down time can be retreieved with the TimeKeyHeldDown function
	UFUNCTION(BlueprintCallable, category = "Input")
		void RegisterKey(FKey Key);

	//How long was this key held down for? Key must be registered first.
	UFUNCTION(BlueprintCallable, BlueprintPure, category = "Input")
		float TimeKeyHeldDown(FKey Key);
};
