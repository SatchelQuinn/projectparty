// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerCharacter.h"
#include "Components/InputComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/GameplayStatics.h"
#include "PlayerActionInterface.h"
//#include "PlayerModifier.h"
#include "UnrealNetwork.h"
#include "PartyController.h"
#include "Engine/World.h"
#include "TimerManager.h"

// Sets default values
APlayerCharacter::APlayerCharacter()
{
	bReplicates = true;

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;

	//Init safe transform
	LastSafeTransform.Add(GetTransform());
}

// Called when the game starts or when spawned
void APlayerCharacter::BeginPlay()
{
	Super::BeginPlay();
	GetWorldTimerManager().SetTimer(FSafeLocationTimer, this, &APlayerCharacter::SetSafeLocation, .9f, true, 1.f);
}

// Called every frame
void APlayerCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void APlayerCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	PlayerInputComponent->BindAxis("MoveForward", this, &APlayerCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &APlayerCharacter::MoveRight);
}

void APlayerCharacter::MoveForward(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);
		const FRotator CameraRot(0,UGameplayStatics::GetPlayerCameraManager(this, 0)->GetCameraRotation().Yaw,0);
		FRotator ActorRot(0, GetActorRotation().Yaw, 0);

		// get forward vector
		//const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		FVector Direction = FVector(1.f, 0.f, 0.f);
		switch (MovementMethod)
		{
		case MM_World:
			break;
		case MM_Camera:
			Direction = FRotationMatrix(CameraRot).GetUnitAxis(EAxis::X);
			break;
		case MM_Perspective:
			Direction = FRotationMatrix(ActorRot).GetUnitAxis(EAxis::X);
		case MM_Tank:
			Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		default:
			break;
		}
		
		AddMovementInput(Direction, Value);
	}
}

void APlayerCharacter::MoveRight(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);
		const FRotator CameraRot(0, UGameplayStatics::GetPlayerCameraManager(this, 0)->GetCameraRotation().Yaw, 0);
		FRotator ActorRot(0, GetActorRotation().Yaw, 0);

		// get right vector 
		//const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		FVector Direction = FVector(0.0f, 1.f, 0.f);
		switch (MovementMethod)
		{
		case MM_World:
			break;
		case MM_Camera:
			Direction = FRotationMatrix(CameraRot).GetUnitAxis(EAxis::Y);
			break;
		case MM_Perspective:
			Direction = FRotationMatrix(ActorRot).GetUnitAxis(EAxis::Y);
		case MM_Tank:
			Direction = FVector::ZeroVector;
			AddControllerYawInput(Value * BaseTurnRate * GetWorld()->GetDeltaSeconds());
			Value = 0.f;
		default:
			break;
		}
		AddMovementInput(Direction, Value);
	}
}

APlayerModifierBase* APlayerCharacter::ReturnClosestInteractActor(FVector &ModifierLocation, TEnumAsByte<EModifierType> &ModifierType)
{
	TSet<AActor*> Overlap;
	APlayerModifierBase* ClosestActor = nullptr;
	AActor* UncastActor = nullptr;
	float distance = 99999.f;
	GetOverlappingActors(Overlap, TSubclassOf<APlayerModifierBase>());
	for (AActor* actor : Overlap)
	{
		bool bImplements = false;
		bImplements = actor->GetClass()->ImplementsInterface(UPlayerActionInterface::StaticClass());
		//IPlayerActionInterface* Interface = Cast<IPlayerActionInterface>(actor);
		if (bImplements)
		{
			bool caninteract = false;
			//caninteract = actor->CanInteract();
			float localdist = FVector::Distance(actor->GetActorLocation(), GetActorLocation());
			if (distance > localdist)
			{
				distance = localdist;
				UncastActor = actor;
			}
		}
	}
	ClosestActor = Cast<APlayerModifierBase>(UncastActor);
	if (ClosestActor)
	{
		ModifierLocation = ClosestActor->GetActorLocation();
		ModifierType = ClosestActor->ModifierType;
	}
	return ClosestActor;
}

void APlayerCharacter::UpdateSpeed()
{
	if (bIsPickingUp)
		GetCharacterMovement()->MaxWalkSpeed = 0.f;
	else
	{
		GetCharacterMovement()->MaxWalkSpeed = BaseSpeed * 100;
	}
}

bool APlayerCharacter::SetPickupStatus(bool NewStatus)
{
	bIsPickingUp = NewStatus;
	UpdateSpeed();
	return NewStatus;
}

FTransform APlayerCharacter::GetSafeTransform()
{
	FTransform FinalTrans = FTransform();
	FHitResult VoidHit;
	FVector StartLocation;
	FVector EndLocation;
	FCollisionQueryParams CollisionParameters;
	float RayLength = 500.f;
	CollisionParameters.AddIgnoredActor(this);
	//Line trace for the floor
	for (FTransform trans : LastSafeTransform)
	{
		StartLocation = trans.GetLocation() + FVector(0.f, 0.f, RayLength / 2);
		EndLocation = trans.GetLocation() - FVector(0.f, 0.f, RayLength / 2);
		GetWorld()->LineTraceSingleByChannel(VoidHit, StartLocation, EndLocation, ECollisionChannel::ECC_WorldStatic, CollisionParameters);
		if (VoidHit.bBlockingHit)
		{
			FinalTrans = trans;
			break;
		}
	}
	return FinalTrans;
}

void APlayerCharacter::SetSafeLocation()
{
	bool isfalling = GetCharacterMovement()->IsFalling();
	if (!isfalling)
	{
		//don't add this new transform if it's similar to the old one
		if (!LastSafeTransform[0].GetLocation().Equals(GetActorLocation(), 5.f))
		{
			if (LastSafeTransform.Num() > 10)
			{
				LastSafeTransform.Pop();
			}
			LastSafeTransform.Insert(GetTransform(), 0);
		}
		bIsAboveGround = true;
	}
	//Raytrace to make sure this character isn't above the void
	if (isfalling)
	{
		FHitResult VoidHit2;
		FVector StartLocation;
		FVector EndLocation;
		FCollisionQueryParams CollisionParameters;
		float RayLength = 1000.f;
		CollisionParameters.AddIgnoredActor(this);
		//Line trace for the floor
		StartLocation = GetActorLocation();
		EndLocation = GetActorLocation() - FVector(0.f, 0.f, RayLength);
		GetWorld()->LineTraceSingleByChannel(VoidHit2, StartLocation, EndLocation, ECollisionChannel::ECC_WorldStatic, CollisionParameters);
		if (VoidHit2.bBlockingHit)
		{
			bIsAboveGround = true;
		}
		else
		{
			bIsAboveGround = false;
		}
	}
}

void APlayerCharacter::GetLifetimeReplicatedProps(TArray< FLifetimeProperty >& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(APlayerCharacter, MovementMethod);
	DOREPLIFETIME(APlayerCharacter, PlayerIndex);
	DOREPLIFETIME(APlayerCharacter, RemainingLives);
};

void APlayerCharacter::InitPlayerValues(int32 Index, int32 Lives)
{
	PlayerIndex = Index;
	RemainingLives = Lives;
}

void APlayerCharacter::InitPawnValues_Implementation(int32 index, int32 Lives, EActionType PrimaryButtonAction, EActionType SecondaryButtonAction)
{
	PlayerIndex = index;
	RemainingLives = Lives;
	PrimaryAction = PrimaryButtonAction;
	SecondaryAction = SecondaryButtonAction;
}

void APlayerCharacter::RegisterModifier_Implementation(APlayerModifierBase* NewMod)
{
	AttachedModifers.AddUnique(NewMod);
}

void APlayerCharacter::DestroyModifiers_Implementation()
{
	for (auto Mod : AttachedModifers)
	{
		Mod->Destroy();
	}
	AttachedModifers.Empty();
}

float APlayerCharacter::TakeDamage(float Damage, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	RemainingLives--;
	if (RemainingLives < 1)
	{
		if (IsPlayerControlled())
		{
			APartyPlayerState* PS = Cast<APartyPlayerState>(GetPlayerState());
			if (PS)
				PS->Eliminate(true);
		}
		else
		{
			
		}
	}
	//Non lethal damage
	else
	{

	}
	return Damage;
}

void APlayerCharacter::VoidOut_Implementation()
{

}

const APartyPlayerState* APlayerCharacter::GetMyPlayerState()
{
	//Should refactor to support CPUs
	return Cast<APartyPlayerState>(GetPlayerState());
}

void APlayerCharacter::HandlePrimaryActionPress(FKey Key)
{
	//APartyController* Controller = Cast<APartyController>(GetController());
	//Controller->RegisterKey(Key);
	switch (PrimaryAction)
	{
	case AT_None:
		break;
	case AT_Pickup:
		OnPickupPressed();
		break;
	case AT_Jump:
		Jump();
		break;
	case AT_Punch:
		OnPunchPressed();
	case AT_Slide:
		break;
	case AT_Dash:
		break;
	default:
		break;
	}

}

void APlayerCharacter::HandlePrimaryActionRelease(FKey Key)
{
	APartyController* Controller = Cast<APartyController>(GetController());
	float keyTime = Controller->TimeKeyHeldDown(Key);
	switch (PrimaryAction)
	{
	case AT_None:
		break;
	case AT_Pickup:
		OnPickupReleased(keyTime);
		break;
	case AT_Jump:
		StopJumping();
		break;
	case AT_Punch:
		OnPunchReleased(keyTime);
		break;
	case AT_Slide:
		break;
	case AT_Dash:
		break;
	default:
		break;
	}
}

void APlayerCharacter::HandleSecondaryActionPress(FKey Key)
{
	//APartyController* Controller = Cast<APartyController>(GetController());
//Controller->RegisterKey(Key);
	switch (SecondaryAction)
	{
	case AT_None:
		break;
	case AT_Pickup:
		OnPickupPressed();
		break;
	case AT_Jump:
		Jump();
		break;
	case AT_Punch:
		OnPunchPressed();
		break;
	case AT_Slide:
		break;
	case AT_Dash:
		break;
	default:
		break;
	}
}

void APlayerCharacter::HandleSecondaryActionRelease(FKey Key)
{
	APartyController* Controller = Cast<APartyController>(GetController());
	float keyTime = Controller->TimeKeyHeldDown(Key);
	switch (SecondaryAction)
	{
	case AT_None:
		break;
	case AT_Pickup:
		OnPickupReleased(keyTime);
		break;
	case AT_Jump:
		StopJumping();
		break;
	case AT_Punch:
		OnPunchReleased(keyTime);
		break;
	case AT_Slide:
		break;
	case AT_Dash:
		break;
	default:
		break;
	}
}

void APlayerCharacter::OnPickupPressed_Implementation()
{
}

void APlayerCharacter::OnPickupReleased_Implementation(float KeyTime)
{
}

void APlayerCharacter::OnPunchPressed_Implementation()
{
}

void APlayerCharacter::OnPunchReleased_Implementation(float KeyTime)
{
}
