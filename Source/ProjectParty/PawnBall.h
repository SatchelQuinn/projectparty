// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PlayerPawn.h"
#include "PawnBall.generated.h"

/**
 * 
 */
UCLASS()
class PROJECTPARTY_API APawnBall : public APlayerPawn
{
	GENERATED_BODY()

	/** Sphere*/
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Sphere, meta = (AllowPrivateAccess = "true"))
	class USphereComponent* SphereCollision;

public:
	APawnBall();

	UPROPERTY(EditAnywhere,BlueprintReadWrite, category = "Sphere")
	float SphereRadius;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, category = "Sphere")
		bool bIsFalling;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, category = "Movement")
		float TorqueIntensity;

protected:

	void CheckFallingStatus();

	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	// End of APawn interface

	void MoveForward(float Value);

	void MoveRight(float Value);

	//void ProcessGravity(float DeltaTime);

public:

	virtual void Tick(float DeltaTime) override;

	virtual void BeginPlay() override;
	
};
