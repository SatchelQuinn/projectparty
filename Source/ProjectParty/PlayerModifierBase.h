// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "EPartyEnums.h"
#include "PlayerModifierBase.generated.h"

UCLASS()
class PROJECTPARTY_API APlayerModifierBase : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	APlayerModifierBase();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Meta = (ExposeOnSpawn = true), Category = "Gameplay")
	APawn* GenericOwner = nullptr;

	//Should this modifier attach itself to it's owner upon spawn from the GameMode?
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item")
		bool bAutoEquip;

	//The type of object this is. Used in the animation state machine. Heavy items typically cannot be picked up.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item")
		TEnumAsByte<EModifierType> ModifierType = MT_Handheld;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void SetGenericOwner(APawn* IncomingOwner);

	//Blueprint event to handle pickup calls
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, category = "Item")
		void Equip();
	void Equip_Implementation();

	//Moves the actor by it's socket offset
	UFUNCTION(BlueprintCallable, category = "Item")
		void GrabOffsetActor(USceneComponent* GrabMesh);

};
