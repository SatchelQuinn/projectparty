// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

//#include "Engine.h"
#include "UObject/Class.h"
#include "CoreMinimal.h"

/**
 * 
class PROJECTPARTY_API EPartyEnums
{
public:
	EPartyEnums();
	~EPartyEnums();
};
*/

UENUM(BlueprintType)
enum EGameType
{
	GT_Local UMETA(DisplayName = "Local"),
	GT_Online UMETA(DisplayName = "Online"),
	GT_Other UMETA(DisplayName = "Other")
};

UENUM(BlueprintType)
enum EActionType
{
	AT_Pickup UMETA(DisplayName = "Pickup/Use"),
	AT_Jump UMETA(DisplayName = "Jump"),
	AT_Punch UMETA(DisplayName = "Punch/Release"),
	AT_Slide UMETA(DisplayName = "Slide"),
	AT_Dash UMETA(DisplayName = "Dash"),
	AT_None UMETA(DisplayName = "No Action")

};

UENUM(BlueprintType)
enum EMovementMethod
{
	MM_Camera UMETA(DisplayName = "Camera Relative"),
	MM_World UMETA(DisplayName = "World Relative"),
	MM_Tank UMETA(DisplayName = "Tank Controls"),
	MM_Perspective UMETA(DisplayName = "Perspective")
};

UENUM(BlueprintType)
enum EUserInterfaceType
{
	UI_None UMETA(DisplayName = "None"),
	UI_Points UMETA(DisplayName = "Points")
};

UENUM(BlueprintType)
enum EObjectiveType
{
	OT_Elimination UMETA(DisplayName = "Elimination"),
	OT_MostPoints UMETA(DisplayName = "Most Points"),
	OT_LeastPoints UMETA(DisplayName = "Least Points"),
	OT_FirstTo UMETA(DisplayName = "First to __")
};

UENUM(BlueprintType)
enum EMiniGames
{
	MG_Lobby UMETA(DisplayName = "Lobby"),
	MG_ChainGang UMETA(DisplayName = "Chain Gang"),
	MG_Rooftop UMETA(DisplayName = "Rooftop"),
	MG_SilentSpook UMETA(DisplayName = "Silent Spook"),
	MG_Custom UMETA(DisplayName = "Custom"),
	MG_Debug UMETA(DisplayName = "Debug")
};

UENUM(BlueprintType)
enum EModifierType
{
	MT_Handheld UMETA(DisplayName = "Handheld"),
	MT_TwoHanded UMETA(DisplayName = "Two-Handed"),
	MT_GunPop UMETA(DisplayName = "Pop Gun"),
	MT_GunCharge UMETA(DisplayName = "Charge Gun"),
	MT_Heavy UMETA(DisplayName = "Heavy")
};