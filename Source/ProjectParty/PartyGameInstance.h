// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "EPartyEnums.h"
#include "Engine/GameInstance.h"
#include "GameplayStructures.h"
#include "PartyGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class PROJECTPARTY_API UPartyGameInstance : public UGameInstance
{
	GENERATED_BODY()
	
public:

	UPartyGameInstance();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TEnumAsByte<EGameType> GameType = GT_Local;

	//An array of data to be preserved 
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Player")
		TArray<FPlayerData> PlayersData;

	//The next minigame in the queue
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Rules")
	TEnumAsByte<EMiniGames> SelectedGame = MG_Lobby;

	//The default rules for each game type
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Rules")
		TMap<TEnumAsByte<EMiniGames>, FRulesStruct> DefaultGameRules;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool bDebugMode;

protected:

public:

	UFUNCTION(BlueprintCallable, Category = "Multiplayer")
		void CallServerTravel(FString LevelURL, bool bAbsolute = false);
};
