// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPawn.h"
#include "UnrealNetwork.h"

// Sets default values
APlayerPawn::APlayerPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	bReplicates = true;
}

// Called when the game starts or when spawned
void APlayerPawn::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void APlayerPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void APlayerPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void APlayerPawn::InitPlayerValues(int32 Index)
{
	PlayerIndex = Index;
}

void APlayerPawn::InitPawnValues_Implementation(int32 index, int32 Lives, EActionType PrimaryButtonAction, EActionType SecondaryButtonAction)
{
	PlayerIndex = index;
	RemainingLives = Lives;
	PrimaryAction = PrimaryButtonAction;
	SecondaryAction = SecondaryButtonAction;
}

void APlayerPawn::GetLifetimeReplicatedProps(TArray< FLifetimeProperty >& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(APlayerPawn, MovementMethod);
	DOREPLIFETIME(APlayerPawn, PlayerIndex);
};