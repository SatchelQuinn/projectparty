// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerModifier.h"
#include "UnrealNetwork.h"

APlayerModifier::APlayerModifier()
{
	ForwardOffset = FVector::ZeroVector;
	SetOwningPawn();
	bReplicates = true;
}

APawn* APlayerModifier::GetOwningPawn()
{
	if (OwningCharacter)
		return OwningCharacter;
	if (OwningPawn)
		return OwningPawn;
	return nullptr;
}


void APlayerModifier::SetOwningPawn()
{
	APlayerCharacter* Character = Cast<APlayerCharacter>(GenericOwner);
	if (Character)
		OwningCharacter = Character;
	else
	{
		APlayerPawn* Pawn = Cast<APlayerPawn>(GenericOwner);
		if (Pawn)
			OwningPawn = Pawn;
	}
}



void APlayerModifier::BeginPlay()
{
	Super::BeginPlay();
	if (GetOwningPawn())
	{
		FVector FinalOffset = ForwardOffset * GetOwningPawn()->GetActorForwardVector();
		SetActorLocation(GetActorLocation() + FinalOffset, true);
	}
}

void APlayerModifier::GetLifetimeReplicatedProps(TArray< FLifetimeProperty >& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(APlayerModifier, OwningPawn);
	DOREPLIFETIME(APlayerModifier, OwningCharacter);
	DOREPLIFETIME(APlayerModifier, PickedUpBy);
};