// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "EPartyEnums.h"
#include "Engine/EngineTypes.h"
#include "GameplayStructures.h"
#include "PartyController.h"
#include "StageManager.h"
#include "ProjectPartyGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class PROJECTPARTY_API AProjectPartyGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FRulesStruct RulesStruct;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<APartyController*> Players;

	UPROPERTY(BlueprintReadOnly)
	TEnumAsByte<EGameType> GameType = GT_Local;

protected:

	//TArray<FTimerHandle> PlayerInitTimerArray;
	//TArray<FTimerDelegate> PlayerInitDelegateArray;
	//TArray<AController*> ControllerInitArray;

public:

	AProjectPartyGameModeBase();
	
	virtual void PostLogin(APlayerController* NewPlayer);

	virtual void GenericPlayerInitialization(AController* C);

	UFUNCTION(BlueprintCallable, category = "Gameplay")
		void SpawnLocalPlayers(int32 AdditionalPlayers);

	//Spawns the correct player pawn with the correct attachments
	UFUNCTION(BlueprintCallable, category = "Gameplay")
		APawn* SpawnPlayerClass(APartyController* Controller);

	//Unloads the correct rules from the game instance
	UFUNCTION(BlueprintCallable, category = "Data Management")
		FRulesStruct UnpackRules();

	//Spawns Player attachments
	UFUNCTION(BlueprintCallable, category = "Gameplay")
		void SpawnPlayerModifiers(APawn* PlayerPawn);

	virtual void BeginPlay();

	void SetSplitscreenStatus(AActor* Context, bool Status);

	//Forces the game mode to initiate its BeginPlay function that is lost during server travel
	UFUNCTION(BlueprintCallable, category = "Multiplayer")
		void PostServerTravel();

	UFUNCTION()
	void ReinitializePlayer(AController* C);

};
