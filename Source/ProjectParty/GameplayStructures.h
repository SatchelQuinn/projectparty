// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "EPartyEnums.h"
#include "UObject/Class.h"
#include "PlayerModifierBase.h"
#include "GameplayStructures.generated.h"

/**
 * 
class PROJECTPARTY_API GameplayStructures
{
public:
	GameplayStructures();
	~GameplayStructures();
};
*/

USTRUCT(BlueprintType)
struct FRulesStruct
{
	GENERATED_BODY()

		FRulesStruct()
	{

	}
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		TEnumAsByte<EMovementMethod> MovementMethod = MM_Camera;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		float TimerLength = 30.f;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		bool bUseSplitscreen = false;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		bool bShowActionAnimation = true;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		TMap<TEnumAsByte<EObjectiveType>, bool> ActiveObjectives;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		TSubclassOf<APawn> PlayerPawn = nullptr;

	//Player Parameters

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		int32 Lives = 1;

	//What should be spawned as an "attachment" to the player?
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		TSubclassOf<APlayerModifierBase> PlayerModifier;

	//Player Controls

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		TEnumAsByte<EActionType> PrimaryButtonAction = AT_Pickup;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		TEnumAsByte<EActionType> SecondaryButtonAction = AT_Jump;
};

USTRUCT(BlueprintType)
struct FPlayerData
{
	GENERATED_BODY()

		FPlayerData()
	{

	}
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	bool bIsActive = false;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	bool bIsCPU = false;
};

USTRUCT(BlueprintType)
struct FGameModifiers
{
	GENERATED_BODY()

		FGameModifiers()
	{

	}

};