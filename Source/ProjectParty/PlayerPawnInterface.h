// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "PlayerModifierBase.h"
#include "EPartyEnums.h"
#include "PlayerPawnInterface.generated.h"

// This class does not need to be modified.
UINTERFACE(BlueprintType)
class UPlayerPawnInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class PROJECTPARTY_API IPlayerPawnInterface
{
	GENERATED_BODY()

		// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Data Management")
		void InitPawnValues(int32 index, int32 Lives, EActionType PrimaryButtonAction, EActionType SecondaryButtonAction);
	virtual void InitPawnValues_Implementation(int32 index, int32 Lives, EActionType PrimaryButtonAction, EActionType SecondaryButtonAction);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Gameplay")
		void MovePlayerTo(FVector Location, FRotator FinalRot);
	void MovePlayerTo_Implementation(FVector Location, FRotator FinalRot);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Gameplay")
		void Celebrate(bool PlayerLost);
	void Celebrate_Implementation(bool PlayerLost);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Data Management")
		void RegisterModifier(APlayerModifierBase* NewMod);
	virtual void RegisterModifier_Implementation(APlayerModifierBase* NewMod);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Data Management")
		void DestroyModifiers();
	virtual void DestroyModifiers_Implementation();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Gameplay")
		void VoidOut();
	virtual void VoidOut_Implementation();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Gameplay")
		void OnEliminate();
	virtual void OnEliminate_Implementation();
};