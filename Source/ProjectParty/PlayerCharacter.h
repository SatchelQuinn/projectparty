// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "EPartyEnums.h"
#include "PlayerPawnInterface.h"
#include "PlayerModifierBase.h"
#include "PartyPlayerState.h"
#include "PlayerCharacter.generated.h"

UCLASS()
class PROJECTPARTY_API APlayerCharacter : public ACharacter, public IPlayerPawnInterface 
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	APlayerCharacter();

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
		float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
		float BaseLookUpRate;

	//What method this character uses for locamotion
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated, category = "Attributes")
		TEnumAsByte<EMovementMethod> MovementMethod;

	//An actor in range to call the interact function
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated, category = "Gameplay")
		AActor* InteractActor;

	//The intract actor we are currently holding. Can only hold one.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated, category = "Gameplay")
		APlayerModifierBase* HeldActor;

	//The index of the player. Used for color coding.
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Replicated, category = "Attributes")
		int32 PlayerIndex;

	//The number of lives the player has. Used for elimination matches
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Replicated, category = "Attributes")
		int32 RemainingLives;

	//Used by the update speed function. Multiplied by 100 to get the final movement speed stat.
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Replicated, category = "Attributes")
		float BaseSpeed = 6.f;

	UPROPERTY(BlueprintReadOnly, category = "Controls")
		TEnumAsByte<EActionType> PrimaryAction = AT_None;

	UPROPERTY(BlueprintReadOnly, category = "Controls")
		TEnumAsByte<EActionType> SecondaryAction = AT_None;

	//The last transform we known to be safe. Initalized to the current transform on construct
	UPROPERTY(BlueprintReadOnly, meta = (category = "Condition"))
		TArray<FTransform> LastSafeTransform;

	//Is set true when this character is standing on the ground, or when a ray trace hits ground beneath them.
	UPROPERTY(BlueprintReadWrite, EditAnywhere, category = "Condition")
		bool bIsAboveGround = false;

	//Is set true when this character is attempting to pick up an object. Cannot be edited in blueprints. Use SetPickupStatus instead.
	UPROPERTY(BlueprintReadOnly, category = "Condition")
		bool bIsPickingUp = false;

	//An array of modifers that are currently attached to the character
	UPROPERTY(BlueprintReadOnly, category = "Attributes")
		TArray<APlayerModifierBase*> AttachedModifers;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	FTimerHandle FSafeLocationTimer;


public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	/** Called for forwards/backward input */
	void MoveForward(float Value);

	/** Called for side to side input */
	void MoveRight(float Value);

	UFUNCTION(BlueprintPure, category = "Gameplay")
		APlayerModifierBase* ReturnClosestInteractActor(FVector& ModifierLocation, TEnumAsByte<EModifierType>& ModifierType);

	//Changes the player's speed stat to match their current state
	UFUNCTION(BlueprintCallable, category = "Gameplay")
		void UpdateSpeed();

	//Returns true if the pickup request was successful and updates the character's speed.
	UFUNCTION(BlueprintCallable, category = "Gameplay")
		bool SetPickupStatus(bool NewStatus);
	
	FTransform GetSafeTransform();

	void SetSafeLocation();

	//Called before the pawn is spawned.
	void InitPlayerValues(int32 Index, int32 Lives);

	//Interface version of InitPlayerValues function
	virtual void InitPawnValues_Implementation(int32 index, int32 Lives, EActionType PrimaryButtonAction, EActionType SecondaryButtonAction) override;

	//Called when the game mode wants to despawn all spawned modifers
	virtual void RegisterModifier_Implementation(APlayerModifierBase* NewMod) override;

	//Called when the game mode wants to despawn all spawned modifers
	virtual void DestroyModifiers_Implementation() override;

	//Damage override
	virtual float TakeDamage(float Damage, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;

	virtual void VoidOut_Implementation() override;

	const APartyPlayerState* GetMyPlayerState();

	//Control functions
	UFUNCTION(BlueprintCallable, category = "Controls")
		void HandlePrimaryActionPress(FKey Key);

	UFUNCTION(BlueprintCallable, category = "Controls")
		void HandlePrimaryActionRelease(FKey Key);

	UFUNCTION(BlueprintCallable, category = "Controls")
		void HandleSecondaryActionPress(FKey Key);

	UFUNCTION(BlueprintCallable, category = "Controls")
		void HandleSecondaryActionRelease(FKey Key);

	UFUNCTION(BlueprintNativeEvent, category = "Controls")
		void OnPickupPressed();
	void OnPickupPressed_Implementation();

	UFUNCTION(BlueprintNativeEvent, category = "Controls")
		void OnPickupReleased(float KeyTime);
	void OnPickupReleased_Implementation(float KeyTime);

	UFUNCTION(BlueprintNativeEvent, category = "Controls")
		void OnPunchPressed();
	void OnPunchPressed_Implementation();

	UFUNCTION(BlueprintNativeEvent, category = "Controls")
		void OnPunchReleased(float KeyTime);
	void OnPunchReleased_Implementation(float KeyTime);


};
